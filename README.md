# lookaside

These files are needed for CKI kernel tests.

Follow these steps to add new files.

```
wget http://example.com/big_file.tgz
git lfs track big_file.tgz
git add .gitattributes big_file.tgz
git commit -m "Added big_file.tgz"
git push
```
